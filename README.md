# Cheat sheet for Solus Linux
<br>

## Enter boot menu

Press ESC right after POST (it is a very small timeframe there)

## Localise TTY

    sudo eopkg install micro
    micro /etc/vconsole.conf

add the following lines and save:

    KEYMAP="ru"
    FONT="ter-u18b"

**Install "terminus" as console font for Solus:**

    sudo eopkg install make
    wget https://sourceforge.net/projects/terminus-font/files/terminus-font-4.46/terminus-font-4.46.tar.gz/download -O terminus.tar.gz
    tar -xvf terminus.tar.gz
    cd terminus-font-4.46
    ./configure --prefix=/usr
    make -j4 psf
    sudo make install-psf

**Optional (without making /etc/vconsole.conf):**

    # add kernel parameter for vconsole.font
    echo 'vconsole.font=ter-u18b' | sudo tee /etc/kernel/cmdline
    
    # append kernel parameter to boot
    sudo clr-boot-manager update

## Map "User" folders to another drive

`/etc/fstab`

    UUID=0db06cee-0f77-4d5d-96c5-bbe0916c3c36   /data       ext4    defaults,noatime,commit=60,barrier=0    0 2
    /data/Archive/           /home/igor/Archive/            none    defaults,bind    0 0
    /data/Calibre_Library/   /home/igor/Calibre_Library/    none    defaults,bind    0 0
    /data/Desktop/           /home/igor/Desktop/            none    defaults,bind    0 0
    /data/Documents/         /home/igor/Documents/          none    defaults,bind    0 0
    /data/Downloads/         /home/igor/Downloads/          none    defaults,bind    0 0
    /data/ELMANK/            /home/igor/ELMANK/             none    defaults,bind    0 0
    /data/Music/             /home/igor/Music/              none    defaults,bind    0 0
    /data/Pictures/          /home/igor/Pictures/           none    defaults,bind    0 0
    /data/Public/            /home/igor/Public/             none    defaults,bind    0 0
    /data/Repos/             /home/igor/Repos/              none    defaults,bind    0 0
    /data/Templates/         /home/igor/Templates/          none    defaults,bind    0 0
    /data/Videos/            /home/igor/Videos/             none    defaults,bind    0 0

## Add systemwide custom path and other

`/etc/profile` - it is a link in Solus, redirects to `usr/share/defaults/etc/profile`

    export PATH="${PATH}:/home/igor/.user-scripts"
    # can use `append_path "/home/igor/.user-scripts"` 
    # in case if "export" goes below
    
    if [ -f /home/igor/.user-scripts/shell-aliases ]; then
        source /home/igor/.user-scripts/shell-aliases
    fi
    
    eval "$(starship init bash)"

## Add systemwide cusom evrironment variables

`/etc/environment`

```
EDITOR=/usr/bin/micro
```

## Set battery charge limit for laptop

We run a custom script on system boot: <br>
`/etc/systemd/system/charge.service `

    [Unit] 
    Description= Set charge limit 
    
    [Service] 
    Type= simple 
    ExecStart= /home/igor/.user-scripts/set-charge-limit
    
    [Install] 
    WantedBy= multi-user.target

Make the service running:

    sudo systemctl enable charge.service
    sudo systemctl start charge.service

**Optional:**

Ensures the threshold value is set correctly even after plug/unplug power cord, also after returm from sleep or hibernate.

`etc/systemd/system/charge.timer`

    [Unit] 
    Description= Runs charge.service
    
    [Timer] 
    OnUnitActiveSec=1min  # no issues with sleep and hibernate
    Unit=charge.service 
    
    [Install] 
    WantedBy=timers.target

Make it running:

    sudo systemctl enable charge.timer
    sudo systemctl start charge.timer

## .desktop

Global application launch files ending ".desktop" go in:

`/usr/share/applications`

Personal application files go in:

`/home/$USER/.local/share/applications/`

## Fonts

- `~/.local/share/fonts` - for user-level fonts
- `/usr/share/fonts` - for system-wide fonts

## PipeWire

1. Enable (without having to run under sudo) the user PipeWire service and socket with: 
   
        systemctl --user enable pipewire

2. When continue with the user PipeWire PulseAudio: 
   
       systemctl --user enable pipewire-pulse

3. And the PipeWire Multimedia Service Session Manager: 
   
        sudo eopkg install wireplumber
        systemctl --user enable wireplumber

4. Disable the PulseAudio socket and service with: 
   
        systemctl --user disable pulseaudio pulseaudio.socket

5. Install the `pipewire-jack` package if you want to test JACK support.

6. Copy PipeWire config files to user space 
   
        cp -r /etc/pipewire/ ~/.config/

7. Edit the config files if required (check your hardware compatibility): 
   
    `~/.config/pipewire/pipewire.conf`
   
        default.clock.rate          = 96000
        default.clock.allowed-rates = [ 48000 96000 192000 44100 88200 176400 ]
   
   the same changes for both (max quality, high CPU load):
   
    `~/.config/pipewire/client.conf` and <br>
    `~/.config/pipewire/pipewire-pulse.conf`
   
        resample.quality      = 15

8. Reboot

## Make a file executable

```
chmod +x <file>
```

### Change a file or folder ownership

```
sudo chown -hR "user":"group" "file_or_dir_name"
```

## Check your Linux system boot time

```
systemd-analyze
```

## systemd boot loader
`options root=PARTUUID=eb64d38e-e027-4bca-b548-d58d717a6858 quiet loglevel=3 splash systemd.show_status=false rw radeon.si_support=0 radeon.cik_support=0 amdgpu.si_support=1 amdgpu.cik_support=1 resume=UUID=9cf96ea1-7cc2-4c35-8d8f-257511dec1f0`


## Usefull links

[**Snap Store** | Install Snap Store on Linux](https://snapcraft.io/snap-store)

[**Flatpak** | SolusQuick Setup](https://www.flatpak.org/setup/Solus)

[**KWin script** | Move Window and Focus to Desktop](https://www.opencode.net/nightreveller/kwin-move-window-and-focus-to-desktop)

[**bismuth** | Making tiling window management easy on KDE Plasma](https://github.com/Bismuth-Forge/bismuth)

[**konsave** | Backup and Restore your KDE Plasma configuration with CLI application](https://github.com/Prayag2/konsave)

[**Libinput-gestures** | Read gestures from touchpad](https://github.com/bulletmark/libinput-gestures/)

[**xdotool** | x11 automation tool](https://github.com/jordansissel/xdotool)

[**GUI gestures** | Modern, minimal GUI app for libinput-gestures](https://gitlab.com/cunidev/gestures)

