
local config = import("micro/config")

config.AddRuntimeFile("igorcolors", config.RTColorscheme, "colorschemes/igor-16-linux.micro")
config.AddRuntimeFile("igorcolors", config.RTColorscheme, "colorschemes/igor-16-optimal.micro")
