[Appearance]
ColorScheme=eighties.dark

[General]
Name=eighties.dark
Parent=FALLBACK/

[Scrolling]
HistorySize=2000
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=true
