[Appearance]
ColorScheme=ArcoArcDark
Font=JetBrainsMono Nerd Font,10,-1,5,50,0,0,0,0,0

[General]
Command=/bin/bash
Name=arcolinux
Parent=FALLBACK/
TerminalColumns=90
TerminalRows=30

[Scrolling]
HistoryMode=2
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=true
