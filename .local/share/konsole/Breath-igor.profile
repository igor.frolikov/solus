[Appearance]
ColorScheme=Breath-igor
Font=FiraCode Nerd Font,11,-1,5,50,0,0,0,0,0

[General]
Command=/bin/zsh
Name=Breath-igor
Parent=FALLBACK/
ShowTerminalSizeHint=false

[Scrolling]
HistorySize=2000
ScrollBarPosition=2
