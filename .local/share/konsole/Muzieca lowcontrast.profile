[Appearance]
ColorScheme=Muzieca lowcontrast

[General]
Name=Muzieca lowcontrast
Parent=FALLBACK/

[Scrolling]
HistorySize=2000
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=true
