[Appearance]
AntiAliasFonts=true
BoldIntense=true
ColorScheme=igor-16-optimal
Font=FiraCode Nerd Font,12,-1,5,50,0,0,0,0,0

[General]
Name=igor-16-optimal
Parent=FALLBACK/

[Scrolling]
HistorySize=2000
ScrollBarPosition=2

[Terminal Features]
BlinkingCursorEnabled=true
